﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class BombController : MonoBehaviour {
	public Sprite explodeSprite;

	// Use this for initialization
	void Start () {
		StartCoroutine(Detonate());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator Detonate() {
		Text countdown = GetComponentInChildren<Text>();
		int target = 3;
		while (target >= 0) {
			countdown.text = target.ToString();
			yield return new WaitForSeconds(1);
			target--;
		}

		GetComponent<SpriteRenderer>().sprite = explodeSprite;
		GetComponent<CircleCollider2D>().enabled = true;
		GetComponent<PointEffector2D>().enabled = true;

		yield return new WaitForSeconds(0.5f);

		Destroy(transform.parent.gameObject);

	}
}
