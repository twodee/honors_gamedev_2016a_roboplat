﻿using UnityEngine;
using System.Collections;

public class BackgroundController : MonoBehaviour {
	public Transform player;
	public float scrollSpeed;

	void Update(){
		transform.position = new Vector3(
			player.transform.position.x,
			player.transform.position.y, 
			transform.position.z);

		MeshRenderer renderer = GetComponent<MeshRenderer>();
		// move offset along with player position
		Vector2 offset = renderer.material.GetTextureOffset("_MainTex");
		renderer.material.SetTextureOffset("_MainTex", new Vector2(scrollSpeed * player.position.x, offset.y));
	}
}
