﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public float maximumSpeed;
  public Transform groundCheck;
  public LayerMask groundLayer;
  /* public Image washerIcon; */
  /* public Text washerCount; */
	public GameObject bombPrefab;

  private int nWashers = 0;
  private bool isJumping;
  private Animator animator;
  private new Rigidbody2D rigidbody;
  private bool isFacingRight;

  private bool isDying;
  private Vector3 reviveAt;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>();
    animator = GetComponent<Animator>();
    isFacingRight = true;
    isJumping = false;
    isDying = false;
    reviveAt = transform.position;
  }

  void Update() {
    if (!isJumping && Input.GetButtonDown("Jump")) {
      rigidbody.AddForce(new Vector2(0.0f, 20000.0f));
    }

		if (Input.GetKeyDown(KeyCode.B)) {
			Instantiate(bombPrefab, transform.position, Quaternion.identity);
		}
  }

  void FixedUpdate() {
    bool isGrounded = Physics2D.OverlapCircle(groundCheck.position, 3.0f, groundLayer);

    if (isJumping && isGrounded) {
      isJumping = false;
      animator.SetBool("IsJumping", false);
    } else if (!isJumping && !isGrounded) {
      isJumping = true;
      animator.SetBool("IsJumping", true);
    }

    float oomph = Input.GetAxis("Horizontal");
    animator.SetFloat("Speed", Mathf.Abs(oomph));
    rigidbody.velocity = new Vector2(oomph * maximumSpeed, rigidbody.velocity.y);

    if ((oomph < 0 && isFacingRight) ||
        (oomph > 0 && !isFacingRight)) {
      Flip();
    }
  }

  void Flip() {
    isFacingRight = !isFacingRight;

    Vector3 scale = transform.localScale;
    scale.x = -scale.x;
    transform.localScale = scale;
  }

  void OnTriggerEnter2D(Collider2D collider){
    if (!isDying && collider.tag == "Deaaath") {
      animator.SetTrigger("IsDead");
      isDying = true;
      rigidbody.isKinematic = true;
      StartCoroutine(Revive());
    } else if (collider.tag == "Checkpoint") {
      reviveAt = transform.position;
    } else if (collider.tag == "Washer") {
      /* StartCoroutine(CollectWasher(collider.gameObject)); */
      collider.enabled = false;
    }
  }

  /* IEnumerator CollectWasher(GameObject washer) { */
    /* float startTime = Time.time; */
    /* float targetTime = 2.0f; */
    /* float elapsedTime; */

    /* // calc start and end */
    /* Vector3 startPositionInWorldSpace = washer.transform.position; */

    /* do { */
      /* elapsedTime = Time.time - startTime; */
      /* float proportion = Mathf.Clamp01(elapsedTime / targetTime); */
      /* // lerp between start and end using proportion */
      /* Vector3 endPositionInScreenSpace = washerIcon.transform.position; */
      /* Vector3 end = Camera.main.ScreenToWorldPoint(new Vector3(endPositionInScreenSpace.x, endPositionInScreenSpace.y, Camera.main.nearClipPlane)); */
      /* washer.transform.position = Vector3.Lerp(startPositionInWorldSpace, end, proportion); */
      /* yield return null; */
    /* } while (elapsedTime < targetTime); */

    /* Destroy(washer); */

    /* nWashers++; */
    /* washerCount.text = nWashers.ToString(); */
  /* } */

  IEnumerator Revive() {
    yield return new WaitForSeconds((3 + 4) * 0.5f);
    rigidbody.position = reviveAt;
    animator.SetTrigger("IsAlive");
    yield return new WaitForFixedUpdate();

    isDying = false;
    rigidbody.isKinematic = false;
  }
}
